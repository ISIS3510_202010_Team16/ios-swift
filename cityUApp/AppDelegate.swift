//
//  AppDelegate.swift
//  cityUApp
//
//  Created by monica bayona on 2/26/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//


import UIKit
import UserNotifications
import Firebase
import GooglePlaces
import FirebaseDatabase
import FirebaseStorage
import Firebase
import FirebaseAuth
import Foundation
import Reachability



let db = Database.database().reference()
let storageRef = Storage.storage().reference()
let storage = Storage.storage()
   var userID = "0"
   var userNegocio = "0"
    var usuario = true
   var userUsuario = "0"
   var personatipo = "-1"



@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    

var sessionManager: FirebaseSession?
    var reachability: Reachability!
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        do {
        try reachability = Reachability()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(_:)), name: Notification.Name.reachabilityChanged, object: reachability)
        try reachability.startNotifier()
        } catch {
            print("This is not working.")
        }
        FirebaseApp.configure()
        
      
        Database.database().isPersistenceEnabled = true
        // Override point for customization after application launch.
        return true
        
        
    }
    
    @objc func reachabilityChanged(_ note: NSNotification) {
    let reachability = note.object as! Reachability
    if reachability.connection != .unavailable {
    if reachability.connection == .wifi {
        sessionManager?.setConectivityState(State: "WIFI")
       
    } else {
        sessionManager?.setConectivityState(State: "MOBILE_DATA")
       
    }
    } else {
        sessionManager?.setConectivityState(State: "NO_INTERNET")
       
    }
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

