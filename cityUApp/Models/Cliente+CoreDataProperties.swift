//
//  Cliente+CoreDataProperties.swift
//  
//
//  Created by CAMILO MARTINEZ on 04/04/2020.
//
//

import Foundation
import CoreData


extension Cliente {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Cliente> {
        return NSFetchRequest<Cliente>(entityName: "Cliente")
    }

    @NSManaged public var nombre: String?
    @NSManaged public var edad: Int16
    @NSManaged public var direccion: String?
    @NSManaged public var pedido: Pedido1?
    @NSManaged public var factura: Factura?

}
