//
//  Factura+CoreDataProperties.swift
//  
//
//  Created by CAMILO MARTINEZ on 04/04/2020.
//
//

import Foundation
import CoreData


extension Factura {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Factura> {
        return NSFetchRequest<Factura>(entityName: "Factura")
    }

    @NSManaged public var id: Int32
    @NSManaged public var fecha: NSDate?
    @NSManaged public var hora: NSDecimalNumber?
    @NSManaged public var precio: NSDecimalNumber?
    @NSManaged public var metodoDePago: String?
    @NSManaged public var pedido: Pedido1?

}
