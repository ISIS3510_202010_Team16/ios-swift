//
//  model.swift
//  cityUApp
//
//  Created by monica bayona on 26/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import Foundation

protocol model: Codable, Identifiable {
}

struct JSON {
    static let encoder = JSONEncoder()
    static let decoder = JSONDecoder()
}

extension Encodable {
    subscript(key: String) -> Any? {
        return dictionary[key]
    }
    var dictionary: [String: Any] {
        return (try? JSONSerialization.jsonObject(with: JSON.encoder.encode(self))) as? [String: Any] ?? [:]
    }
}
