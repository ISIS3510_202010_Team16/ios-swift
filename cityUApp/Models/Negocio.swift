/*
See LICENSE folder for this sample’s licensing information.

Abstract:
The model for an individual landmark.
*/

import SwiftUI
import CoreLocation
import SwiftUI
import FirebaseDatabase

struct Negocio: model{
    var id: String
    var name: String
    var imageName: String
    var tiempo: Int
    var calificacion: Int
    var category: Category
   
   

   

    enum Category: String, CaseIterable, Codable, Hashable {
        case restaurante = "restaurantes"
        case bar = "bares"
        case postres = "postres"
        case supermercado = "supermercados"
        case cafes = "cafes"
        case panaderias = "panaderias"
    }
    
   
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let nombre = value["nombre"] as? String,
            let image = value["image"] as? String,
            let categoria = value["categoria"] as? String,
            let tiempoEspera = value["tiempoEspera"] as? Int,
            let calificacion = value["calificacion"] as? Int
            else {
                return nil
            }
        
        self.name = nombre
        self.tiempo = tiempoEspera
        self.category = Category(rawValue: categoria)!
        self.id = snapshot.key
        self.imageName = image
        self.calificacion = calificacion
       
                      
        
    }
    
    
}





struct Negocio_Previews: PreviewProvider {
    @available(iOS 13.0.0, *)
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
