//
//  Local+CoreDataProperties.swift
//  
//
//  Created by CAMILO MARTINEZ on 04/04/2020.
//
//

import Foundation
import CoreData


extension Local {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Local> {
        return NSFetchRequest<Local>(entityName: "Local")
    }
     @NSManaged public var id: String?

    @NSManaged public var nombre: String?
    @NSManaged public var rangoPrecio: String?
    @NSManaged public var direccion: String?
    @NSManaged public var apertura: String?
    @NSManaged public var cierre: String?
    @NSManaged public var tiempoColaPromedio: Int16
    @NSManaged public var zona: Zona
    @NSManaged public var producto: Producto?
    @NSManaged public var pedido: Pedido1?

}

