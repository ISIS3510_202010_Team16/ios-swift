//
//  User.swift
//  TODO
//
//  Created by Sebastian Esser on 9/18/19.
//  Copyright © 2019 Sebastian Esser. All rights reserved.
//
import Foundation

struct  User : Codable  {
   
    
    var id: String
    var email: String?
    var nombre: String?
    var apellido: String?
    var edad: Int?
    var sexo: String?
    
}
