//
//  Pedido+CoreDataProperties.swift
//  
//
//  Created by CAMILO MARTINEZ on 04/04/2020.
//
//

import Foundation
import CoreData


extension Pedido1 {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pedido1> {
        return NSFetchRequest<Pedido1>(entityName: "Pedido")
    }

    @NSManaged public var id: Int32
    @NSManaged public var horaCreada: NSDecimalNumber?
    @NSManaged public var horaEntregada: NSDecimalNumber?
    @NSManaged public var producto: Producto?

}
