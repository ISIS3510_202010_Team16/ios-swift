/*
See LICENSE folder for this sample’s licensing information.

Abstract:
The model for an individual landmark.
*/


import CoreLocation
import FirebaseDatabase

struct Promocion: model {
   
    
    var id: String
    var producto: String
    var descripcion: String
    var restaurante: String
  
    var costo: Int
  
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let restaurante = value["restaurante"] as? String,
            let producto = value["producto"] as? String,
            let descripcion = value["descripcion"] as? String,
            let costo = value["costo"] as? Int
            else {
                return nil
            }
        
        self.restaurante = restaurante
        self.descripcion  = descripcion
        self.id = snapshot.key
        self.costo = costo
        self.producto = producto
       
       
                      
        
    }
   
   

}
