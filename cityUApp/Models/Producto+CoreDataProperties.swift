//
//  Producto+CoreDataProperties.swift
//  
//
//  Created by CAMILO MARTINEZ on 04/04/2020.
//
//

import Foundation
import CoreData


extension Producto {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Producto> {
        return NSFetchRequest<Producto>(entityName: "Producto")
    }

    @NSManaged public var nombre: String?
    @NSManaged public var precio: Double
    @NSManaged public var stock: Int16
    @NSManaged public var promocion: Bool
    @NSManaged public var descuento: Double
    @NSManaged public var combo: Producto?

}
