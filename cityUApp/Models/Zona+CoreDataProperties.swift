//
//  Zona+CoreDataProperties.swift
//  
//
//  Created by CAMILO MARTINEZ on 04/04/2020.
//
//

import Foundation
import CoreData


extension Zona {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Zona> {
        return NSFetchRequest<Zona>(entityName: "Zona")
    }

    @NSManaged public var nombre: String?
    @NSManaged public var rango: String?
    @NSManaged public var zonaN: String?
    @NSManaged public var zonaS: String?
    @NSManaged public var zonaE: String?
    @NSManaged public var zonaO: String?

}
