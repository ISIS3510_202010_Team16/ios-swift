//
//  FirebaseSession.swift
//  TODO
//
//  Created by Sebastian Esser on 9/18/19.
//  Copyright © 2019 Sebastian Esser. All rights reserved.
//
import SwiftUI
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class FirebaseSession: ObservableObject {
    
    //MARK: Properties
    @Published var session: User?
    @Published var restaurante: String?
    @Published var conectivityState: String?
    @Published var isLoggedUsuario: Bool
  var handle: AuthStateDidChangeListenerHandle?
    @Published var items = [String: Negocio]()
    var dataManager : FilesManager = FilesManager()
    @Published var promociones = [String: Promocion]()
    @Published var productos = [String: ProductoModel]()
    @Published var calificaciones = [String: Calificacion]()
        
    @Published var pedidos: [Pedido] = []
   
    let db = Database.database().reference()
    
   
    
    init() {
        self.isLoggedUsuario = false
           handle = Auth.auth().addStateDidChangeListener { (auth, user) in
               if let user = user {
                  self.isLoggedUsuario = true
                self.getRestaurantes()
                self.getPromociones()
                self.getCalificaciones()
                self.getProductos()
                   
               } else {
                     self.isLoggedUsuario = false
               }
           }
       }

    var ref: DatabaseReference = Database.database().reference(withPath: "\(String(describing: Auth.auth().currentUser?.uid ?? "Error"))")
    
    //MARK: Functions
    func listen() {
        _ = Auth.auth().addStateDidChangeListener { (auth, user) in
            if let user = user {
                
                self.isLoggedUsuario = true
                userID = Auth.auth().currentUser!.uid
                self.getPedidos(userID: userID)
            } else {
                self.isLoggedUsuario = false
                
            }
        }
    }
    
    func setConectivityState(State: String){
        conectivityState = State
    }
    
 
 
 
    
    
    
      func logIn(email: String, password: String, handler: @escaping AuthDataResultCallback) {
          Auth.auth().signIn(withEmail: email, password: password, completion: handler)
      }
      
    
    func logOut() {
            try! Auth.auth().signOut()
        self.isLoggedUsuario = false
           

    }
    
    func setRes(res:String) {
           
        self.restaurante = res
           

    }
    
    func getData(){
        self.getRestaurantes()
                       self.getPromociones()
        self.getCalificaciones()
        self.getProductos()
        print(calificaciones)
        print(promociones)
        
        
    }
    
    func checkLoginStatus() -> Bool {
        
        return Auth.auth().currentUser != nil
            
    }
    
 
    
    func registrarUsuarioDatos (email: String, password: String, nombre: String, apellido: String,sexo: String, edad: Int, negocio: Bool)
    
    {
        let db = Database.database().reference()
        var mensaje = ""
        
     
                 
                          
        
    
             print("You have successfully signed up")
            userID = Auth.auth().currentUser!.uid
           
            let newUser = User(id: userID,  email : email, nombre : nombre, apellido: apellido, edad: edad, sexo: sexo)
                 
                 do {
                     let jsonEncoder = JSONEncoder()
                                              let jsonData = try jsonEncoder.encode(newUser)
                     
                     let json = String(data: jsonData, encoding: String.Encoding.utf16)!.data(using: .utf8)
                    self.dataManager.createFileToURL(withData: json, withName: "UserInfo.json")

                                       // Decode
                                       let jsonDecoder = JSONDecoder()
                                       let secondUser = try jsonDecoder .decode(User.self, from: jsonData)
                 } catch {
                     print("Invalid Selection.")
                 }
                  
            
            db.child("users/\(userID)/perfil/nombre").setValue(nombre)
            
            db.child("users/\(userID)/perfil/apellido").setValue(apellido)
            
            db.child("users/\(String(describing: userID))/perfil/edad").setValue(edad)
            
            db.child("users/\(String(describing: userID))/perfil/sexo").setValue(sexo)
    
            
            
                db.child("users/\(String(describing: userID))/perfil/esNegocio").setValue("0")
                print("irHome")
               
                
                
            self.dataManager.createDirectory(withFolderName: "UserData", toDirectory: .applicationSupportDirectory)
            
             self.isLoggedUsuario = true
             
                
                personatipo = "0"
            
            print(self.isLoggedUsuario)
                
            
            
             
             //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
             
             
         
        
     
        
    }
    
    
    
    
    func registrarCalificacion (titulo: String, descripcion: String, calificacion: String)
    
    {
        let db = Database.database().reference()
        var mensaje = ""
        
     
                 
                          
        let uuid = UUID().uuidString
    
    
            
            db.child("Calificaciones/\(uuid)/descripcion").setValue(descripcion)
        db.child("Calificaciones/\(uuid)/restaurante").setValue(self.restaurante)
            
            db.child("Calificaciones/\(uuid)/id").setValue(uuid)
            
            db.child("Calificaciones/\(uuid)/puntaje").setValue(calificacion)
            
            db.child("Calificaciones/\(uuid)/titulo").setValue(titulo)
        db.child("Calificaciones/\(uuid)/uid").setValue(uuid)
    
            
            
            
             
             //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
             
             
         
        
     
        
    }
    
    
    
    
      func signUp(
          email: String,
          password: String,
          handler: @escaping AuthDataResultCallback
      ) {
          Auth.auth().createUser(withEmail: email, password: password, completion: handler)
      }
    
    func registrarLocal (email: String, password: String, nombre: String, ubicacion: String,numeroLocal: String, categoria: String) -> String
    
    {
        
        var mensaje = ""
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            print(error?.localizedDescription as Any)
            print("paso")
            mensaje = error?.localizedDescription as! String
         if error == nil {
             print("You have successfully signed up")
            userID = Auth.auth().currentUser!.uid
           
            
            self.db.child("negocios/\(userID)/perfil/nombre").setValue(nombre)
            
            self.db.child("negocios/\(userID)/perfil/Ubicacion").setValue(ubicacion)
            
            self.db.child("negocios/\(String(describing: userID))/perfil/numeroLocal").setValue(numeroLocal)
            
            self.db.child("negocios/\(String(describing: userID))/perfil/categoria").setValue(categoria)
    
            
           
            self.db.child("users/\(String(describing: userID))/perfil/esNegocio").setValue("1")
               
                
                
                
               
            
          
            
             
             //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
             
             
         } else {
            
            mensaje = error?.localizedDescription as! String
           
        print("paso")
            print(mensaje)
            
            
            
        }
        
        }
        
        return mensaje
        
    }
    
    
    func getNegocios() {
        ref.observe(DataEventType.value) { (snapshot) in
            self.items = [String: Negocio]()
          
        }
    }
    
    func getCalificaciones() {
        
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "HH':'mm"
              
              let formatter2 = DateFormatter()
            formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        db.child("Calificaciones").observe(.value, with: { snapshot in
            self.calificaciones = [String: Calificacion]()
              
             
              for child in snapshot.children {
             
             
              let snapshot2 = child as? DataSnapshot
             
             
               if let snapshot = child as? DataSnapshot,
                let calificacion = Calificacion(snapshot: snapshot)
                  
               {
                
                
                
                   
                   print(calificacion)
                self.calificaciones[calificacion.id] = calificacion
                
                }
                 
                  
                  
              
             }
         
             
           })
        
        print(items)

    }
    
    func getPromociones() {
        
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "HH':'mm"
              
              let formatter2 = DateFormatter()
            formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        db.child("promociones").observe(.value, with: { snapshot in
            self.promociones = [String: Promocion]()
              
             
              for child in snapshot.children {
             
             
              let snapshot2 = child as? DataSnapshot
             
             
               if let snapshot = child as? DataSnapshot,
                let promocion = Promocion(snapshot: snapshot)
                  
               {
                
                
                
                   
                   print(promocion)
                self.promociones[promocion.id] = promocion
                
                }
                 
                  
                  
              
             }
         
             
           })
        
        print(items)

    }
    
    func getProductos() {
        
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "HH':'mm"
              
              let formatter2 = DateFormatter()
            formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        db.child("Productos").observe(.value, with: { snapshot in
            
            self.productos = [String: ProductoModel]()
              
             
              for child in snapshot.children {
             
             
              let snapshot2 = child as? DataSnapshot
             
             
               if let snapshot = child as? DataSnapshot,
                let promocion = ProductoModel(snapshot: snapshot)
                  
               {
                
                
                
                   
                   print(promocion)
                self.productos[promocion.id] = promocion
                
                }
                 
                  
                  
              
             }
         
             
           })
        
        print(items)

    }
    
    func getRestaurantes() {
        
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "HH':'mm"
              
              let formatter2 = DateFormatter()
            formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        db.child("Restaurantes").observe(.value, with: { snapshot in
            self.items = [String: Negocio]()
              
             
              for child in snapshot.children {
             
             
              let snapshot2 = child as? DataSnapshot
             
             
               if let snapshot = child as? DataSnapshot,
                let negocio = Negocio(snapshot: snapshot)
                  
               {
                
                
                
                   
                   
                self.items[negocio.id] = negocio
                
                }
                 
                  
                  
              
             }
         
             
           })
        
        print(items)

    }
    
    func getPedidos(userID : String) {
        
        
              let dateFormatter = DateFormatter()
                     dateFormatter.dateFormat = "HH':'mm"
              
              let formatter2 = DateFormatter()
                             formatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
                db.child("users/\(userID)/pastillasLista").queryLimited(toLast: 30).observe(.value, with: { snapshot in
              
            
             for child in snapshot.children {
              let value1 = snapshot.value as? [String: AnyObject]
           
             
              let snapshot2 = child as? DataSnapshot
              let id = snapshot2?.key
        
              
               if let snapshot = child as? DataSnapshot,
                  
                  let value = snapshot.value as? [String: AnyObject],
                  let id = value["pedido"] as? String,
                let producto = value["producto"] as? String,
                  let horaString = value["horaCreada"] as? String,
                  let estado = value["tiempoFalta"] as? Int,
                  
                let hora = dateFormatter.date (from: horaString){}
                   
                  
                  
                 
                  
                  
              
             }
         
             
           })

    }
    
    func uploadTODO(todo: String) {
        //Generates number going up as time goes on, sets order of TODO's by how old they are.
        let number = Int(Date.timeIntervalSinceReferenceDate * 1000)
        
      
        //let post = Negocio(todo: todo, isComplete: "false")
        //postRef.setValue(post.toAnyObject())
    }
    
    func updateTODO(key: String, todo: String, isComplete: String) {
        let update = ["todo": todo, "isComplete": isComplete]
        let childUpdate = ["\(key)": update]
        ref.updateChildValues(childUpdate)
    }
    
}


extension AuthErrorCode {
    var description: String? {
        switch self {
        case .emailAlreadyInUse:
            return "Este correo ya está siendo usado por otro usuario"
        case .userDisabled:
            return "Este usuario ha sido deshabilitado"
        case .operationNotAllowed:
            return "Operación no permitida"
        case .invalidEmail:
            return "Correo electrónico no valido"
        case .wrongPassword:
            return "Contraseña incorrecta"
        case .userNotFound:
            return "No se encontró cuenta del usuario con el correo especificado"
        case .networkError:
            return "No hay conexion a internet"
        case .weakPassword:
            return "Contraseña muy debil o no válida"
        case .missingEmail:
            return "Hace falta registrar un correo electrónico"
        case .internalError:
            return "Error interno"
        case .invalidCustomToken:
            return "Token personalizado invalido"
        case .tooManyRequests:
            return "Ya se han enviado muchas solicitudes al servidor"
        default:
            return nil
        }
    }
}

   



public extension Error {
    var localizedDescription: String {
        let error = self as NSError
        if error.domain == AuthErrorDomain {
            if let code = AuthErrorCode(rawValue: error.code) {
                if let errorString = code.description {
                    return errorString
                }
            }
        }
        
        return error.localizedDescription
    } }
