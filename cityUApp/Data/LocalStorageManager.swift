//
//  LocalStorageManager.swift
//  cityUApp
//
//  Created by monica bayona on 26/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import Foundation

struct LocalStorageManager {
    
    static private let defaults = UserDefaults.standard;
    
    static func existe(key:String) -> Bool {
        if defaults.object(forKey: key) != nil {
            return true;
        } else {
            return false;
        }
    }
    
    static func set(key:String, value:Any) {
        defaults.set(value, forKey: key);
        defaults.synchronize()
    }
    
    static func setModel<modelBase:model>(key:String, entity:modelBase) {
        defaults.set(entity.dictionary, forKey: key);
        defaults.synchronize()
    }
    
    static func updateModel<modelBase:model>(key:String, lstEntity:[modelBase]) {
        defaults.set(lstEntity.map{ e in e.dictionary } , forKey: key);
        defaults.synchronize()
    }
    
    static func getInt(key:String) -> Int? {
        if existe(key: key) {
            return defaults.integer(forKey: key);
        } else {
            return nil;
        }
    }
    
    static func getString(key:String) -> String? {
        if existe(key: key) {
            return defaults.string(forKey: key);
        } else {
            return nil;
        }
    }
    
    static func getModel<modelBase:model>(key:String) -> modelBase? {
        if existe(key: key) {
            let info = defaults.dictionary(forKey: key)!;
            let jsonData = (try? JSONSerialization.data(withJSONObject: info, options: []))!;
            return (try? JSON.decoder.decode( modelBase?.self, from: jsonData));
        } else {
            return nil;
        }
    }
    
    static func updateModel<modelBase:model>(key:String) -> [modelBase?]? {
        if existe(key: key) {
            let info = defaults.array(forKey: key)!;
            return info.map {e in
                let jsonData = (try? JSONSerialization.data(withJSONObject: e, options: []))!;
                return (try? JSON.decoder.decode( modelBase?.self, from: jsonData));
            }.filter { e in e != nil }
        } else {
            return nil;
        }
    }
    
}
