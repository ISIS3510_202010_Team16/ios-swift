/*
See LICENSE folder for this sample’s licensing information.

Abstract:
A view showing a scrollable list of landmarks.
*/

import SwiftUI

struct RestauranteRow: View {
   
     @EnvironmentObject var session: FirebaseSession
   
    
    

      
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Restaurantes")
                    .font(.headline)
                    .padding(.leading, 15)
                    .padding(.top, 5)
                NavigationLink(
                    destination: categoryRestaurantView()
                ) {
                    Text("Ver")
                        .font(.body)
                        .foregroundColor(Color("azul"))
                        .multilineTextAlignment(.trailing)
                }
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 10) {
                    ForEach(session.items.keys.sorted(), id: \.self) { index in
                        HStack {
                      NavigationLink(
                          destination: NegocioDetail(
                            negocio: self.session.items[index]!
                          )
                      ) {
                        CategoryItem(   negocio: self.session.items[index]!)
                            .padding(.all, 3.0)
                      }
                            
                        }
                    }
                        
                    }
                    
                
                }
            }
            .frame(height: 185).onAppear(perform: {self.session.getData()})
        }
    }


struct CategoryItem: View {
    @Environment(\.imageCache) var cache: ImageCache
    var negocio: Negocio
    var body: some View {
        
       VStack(alignment: .leading) {
        Image(negocio.imageName)
        .resizable().renderingMode(.original)
                              .cornerRadius(15)
                              .frame(width: 150.0, height: 110.0).background(Color(.white)).cornerRadius(15)
        
        Text(negocio.name)
            .font(.subheadline)
            .foregroundColor(Color.black)
            .multilineTextAlignment(.leading)
            
        Text("Tiempo: \(negocio.tiempo/60)  minutos")
            .foregroundColor(Color("naranja"))
            .multilineTextAlignment(.leading)
            
        
        Text("Calificacion: \(negocio.calificacion)")
            .font(.footnote)
            .foregroundColor(Color.gray)
            .multilineTextAlignment(.leading)
                       
                       
          
            
       }
       .frame(width: 140.0)
        
        
    }
}

struct CategoryRow_Previews: PreviewProvider {
    static var previews: some View {
       Text("")
        
       
    }
}
