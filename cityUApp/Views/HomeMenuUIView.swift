//
//  HomeMenuUIView.swift
//  cityUApp
//
//  Created by monica bayona on 8/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI

struct HomeMenuUIView: View {
    
    @EnvironmentObject var session: FirebaseSession
   
    
   
    
    var body: some View {
        
        
     
            
        VStack {
            ZStack {
            
                Image("Grupo 8773").resizable().frame(height: 150.0).edgesIgnoringSafeArea(.all)
                
                Text("City U Market")
                    .font(.title)
                    .fontWeight(.semibold)
                    .foregroundColor(Color.white).frame( height: 90 )
                
                
                
            
                
            }.frame(height: 90.0)
            
            
            RestauranteRow()
                .padding(.vertical)
            PromocionRows(negocio: "ALL")
                .padding(.vertical, 9.0)
            
           Spacer()
             Spacer()
             Spacer()
                
            }.navigationBarItems(trailing: Button(action: {
                self.session.logOut()
            }) {
                Text("Logout")
            })
            
           
           
            
        
            
        }
        
    }
 




struct HomeMenuUIView_Previews: PreviewProvider {
    static var previews: some View {
        HomeMenuUIView()
    }
}
