//
//  signUp2ViewController.swift
//  MEDICAPP
//
//  Created by monica bayona on 12/12/19.
//  Copyright © 2019 Paola Bayona. All rights reserved.
//

import UIKit
import Eureka
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SwiftUI
import ImageRow
import Foundation
import MapKit


struct SignUpView: View {
   
      @EnvironmentObject var session: FirebaseSession
    @State private var actionState: Int? = 0
    
    
    var body: some View {
        
        NavigationView {
           
            Group {
               if session.isLoggedUsuario == false {
        
        
        ZStack {
        
            
                    
         Image("imageCity")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                 .edgesIgnoringSafeArea(.all)
         
            VStack() {
                
                Text("Registrarse").font(.title).fontWeight(.bold).foregroundColor(Color("naranja")).multilineTextAlignment(.center).padding([.top, .leading, .trailing], 40.0)
            
                SignUpController().padding(.all, 13.0).frame(height: 430.0)
                
                                 
                
               // NavigationLink(destination: HomeMenuUIView(),isActive: self.$session.isLoggedUsuario) {
               //   Text("")
            //    }.hidden()
            }.frame(width: 310.0)
        .background(Color(.white)).cornerRadius(15).offset(y: -95)
         
            
        }
    
    }
                else {
                HomeMenuUIView().onAppear(perform:{ self.session.getData()})
                     .navigationBarItems(trailing: Button(action: {
                                               self.session.logOut()
                                           }) {
                                               Text("Cerrar Sesion")
                                           })
                }
    
            }
            
        }
        
    }
 
}



struct SignUpController: UIViewControllerRepresentable {
    
   

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: Context) -> signUpViewController {
        let imagePickerController = signUpViewController()
        
        return imagePickerController
    }
    
    func updateUIViewController(_ uiViewController: signUpViewController, context: Context) {
        
    }
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var parent: SignUpController
        
        init(_ imagePickerController: SignUpController) {
            self.parent = imagePickerController
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            picker.dismiss(animated: true, completion: nil)
        }
    }
}






class signUpViewController: FormViewController {
    
    struct FormItems {
        static let nombre = "name"
        static let edad = "edad"
        
        static let apellido = "apellido"
        static let sexo = "sexo"
        static let contraseña = "contraseña"
        static let email = "email"
        static let negocio = "negocio"
        
        static let nombreLocal = "nombreLocal"
        static let contraseñaLocal = "contraseñaLocal"
               static let emailLocal = "emailLocal"
    static let categoria = "categoria"
        static let idLocal = "idLocal"
      
        static let ubicacion = "ubicacion"
        static let rutaImagen = "rutaImagen"
    }
    
 
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let required = RuleRequired<String>(msg: "Ingrese un valor")
        var min6 = RuleMinLength(minLength: 6);
                     min6.validationError = ValidationError(msg: "Debe contener al menos 6 caracteres.")
        var min2 = RuleMinLength(minLength: 2);
                            min2.validationError = ValidationError(msg: "Debe contener al menos 2 caracteres.")
               var max25 = RuleMaxLength(maxLength: 25);
               max25.validationError = ValidationError(msg: "Debe contener menos de 25 caracteres.")
        var max30 = RuleMaxLength(maxLength: 30);
        max30.validationError = ValidationError(msg: "Debe contener menos de 30 caracteres.")
        
        
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        form +++ Section("")
            

        
           
           
            <<< TextRow(FormItems.nombre) {
                $0.title = "Nombre"
                $0.add(rule: required)
                $0.add(rule: min2)
                $0.add(rule: max25)
                $0.validationOptions = .validatesOnChange
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                            let labelRow = LabelRow() {
                                $0.title = validationMsg
                                $0.cell.height = { 60 }
                            }.cellUpdate { cell, _ in
                                cell.textLabel?.numberOfLines = 2
                                cell.detailTextLabel?.numberOfLines = 2
                            }
                            let indexPath = row.indexPath!.row + index + 1
                            row.section?.insert(labelRow, at: indexPath)
                        }
                    }
            }
           
            <<< TextRow(FormItems.apellido) {
                $0.title = "Apellido"
                $0.add(rule: required)
                $0.add(rule: min2)
                $0.add(rule: max25)
                $0.validationOptions = .validatesOnChange
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                            let labelRow = LabelRow() {
                                $0.title = validationMsg
                                $0.cell.height = { 60 }
                            }.cellUpdate { cell, _ in
                                cell.textLabel?.numberOfLines = 2
                                cell.detailTextLabel?.numberOfLines = 2
                            }
                            let indexPath = row.indexPath!.row + index + 1
                            row.section?.insert(labelRow, at: indexPath)
                        }
                    }
            }
            
            
            
    
                   
            <<< PickerInputRow<Int>(FormItems.edad){
                        $0.title = "Edad"
                        $0.options = []
                        for i in 0...120{
                            $0.options.append(i)
                        }
                        $0.value = $0.options.first
                    }
    
            <<< PickerInputRow<String>(FormItems.sexo){
                        $0.title = "Sexo"
                        $0.options = ["m","f"]
                        
                        $0.value = $0.options.first
                    }
            
            
        
       <<< TextRow(FormItems.email) {
                 $0.title = "Email"
                 $0.add(rule: required)
                    $0.add(rule: min2)
                       $0.add(rule: max30)
                 $0.validationOptions = .validatesOnChange
                 }
                 .cellUpdate { cell, row in
                     if !row.isValid {
                         cell.titleLabel?.textColor = .red
                     }
                 }
                 .onRowValidationChanged { cell, row in
                     let rowIndex = row.indexPath!.row
                     while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                         row.section?.remove(at: rowIndex + 1)
                     }
                     if !row.isValid {
                         for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                             let labelRow = LabelRow() {
                                 $0.title = validationMsg
                                 $0.cell.height = { 60 }
                             }.cellUpdate { cell, _ in
                                 cell.textLabel?.numberOfLines = 2
                                 cell.detailTextLabel?.numberOfLines = 2
                             }
                             let indexPath = row.indexPath!.row + index + 1
                             row.section?.insert(labelRow, at: indexPath)
                         }
                     }
             }
            
            
          
            
        <<< PasswordRow(FormItems.contraseña) {
            $0.title = "Contraseña"
            $0.add(rule: min6)
            $0.add(rule: max25)
            $0.add(rule: required)
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            .onRowValidationChanged { cell, row in
                let rowIndex = row.indexPath!.row
                while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                    row.section?.remove(at: rowIndex + 1)
                }
                if !row.isValid {
                    for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                        let labelRow = LabelRow() {
                            $0.title = validationMsg
                            $0.cell.height = { 60 }
                        }.cellUpdate { cell, _ in
                            cell.textLabel?.numberOfLines = 2
                            cell.detailTextLabel?.numberOfLines = 2
                        }
                        let indexPath = row.indexPath!.row + index + 1
                        row.section?.insert(labelRow, at: indexPath)
                    }
                }
        }

        <<< PasswordRow() {
            $0.title = "Confirmar contraseña"
            $0.add(rule: RuleEqualsToRow(form: form, tag: FormItems.contraseña, msg: "Las contraseñas no coinciden"))
            }
            .cellUpdate { cell, row in
                if !row.isValid {
                    cell.titleLabel?.textColor = .red
                }
            }
            .onRowValidationChanged { cell, row in
                let rowIndex = row.indexPath!.row
                while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                    row.section?.remove(at: rowIndex + 1)
                }
                if !row.isValid {
                    for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                        let labelRow = LabelRow() {
                            $0.title = validationMsg
                            $0.cell.height = { 60 }
                        }.cellUpdate { cell, _ in
                            cell.textLabel?.numberOfLines = 2
                            cell.detailTextLabel?.numberOfLines = 2
                        }
                        let indexPath = row.indexPath!.row + index + 1
                        row.section?.insert(labelRow, at: indexPath)
                    }
                }
        }

            
        <<< ButtonRow { row in
            row.title = "Registrarse"
            }.cellSetup() {cell, row in
                
              cell.tintColor = UIColor.blue
            }.onCellSelection({ [unowned self] (cell, row) in
                row.section?.form?.validate();
                if let nombre2 = self.form.rowBy(tag: FormItems.nombre) as? RowOf<String>,
                    let nombre = nombre2.value,
                    let email2 = self.form.rowBy(tag: FormItems.email) as? RowOf<String>,
                    let email = email2.value,
                    let password2 = self.form.rowBy(tag: FormItems.contraseña) as? RowOf<String>,
                    let password = password2.value,
                    let apellido2 = self.form.rowBy(tag: FormItems.apellido) as? RowOf<String>,
                    let apellido = apellido2.value,
                    let edad2 = self.form.rowBy(tag: FormItems.edad) as? RowOf<Int>,
                    let edad = edad2.value,
                    let sexo2 = self.form.rowBy(tag: FormItems.sexo) as? RowOf<String>,
                    let sexo = sexo2.value
                  
            
                    
                    
                {
                    
                    if self.form.validate().isEmpty {
                        
                       
                        
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                                     let session = appDelegate.sessionManager
                            session?.signUp(email: email, password: password){ (result, error) in
                                if result == nil {
                                     print("Paso")
                                    
                                    let alertController = UIAlertController(title: "No se pudo registrar el usuario", message: error?.localizedDescription, preferredStyle: .alert)
                                      
                                      let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                      alertController.addAction(defaultAction)
                                      
                                    
                                          if self.presentedViewController == nil {
                                              self.present(alertController, animated: true, completion: nil)
                                          }
                                          else {
                                              self.dismiss(animated: false, completion: nil)
                                              self.present(alertController, animated: true, completion: nil)
                                          }
                                   
                                } else {
                                    
                                    session?.registrarUsuarioDatos(email: email, password: password, nombre: nombre, apellido: apellido, sexo: sexo, edad: edad,  negocio: true)
                                    
                                     session?.isLoggedUsuario = true
                                   
                                }
                            }

                            
                           
                            
                    
                                                  }
                    
                        
                  }
                
                   
                    
                    
                    
                    
                    
                    
                  //  birthDateRow.value = Date(timeInterval: -900*365*86400, since: Date())
                 //   birthDateRow.updateCell()
                    
                  //  likeRow.value = true
                 //   likeRow.updateCell()
                    
                //    row.disabled = .function([FormItems.name]) { form in
               //         (form.rowBy(tag: FormItems.name) as? RowOf<String>)?.value == "Yoda"
              //      }
              //      row.evaluateDisabled()
                }
                    
            } )

                    
                    
                    
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



