//
//  InformacionView.swift
//  cityUApp
//
//  Created by monica bayona on 26/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI

struct InformacionView: View {
    var negocio: Negocio
    var body: some View {
        VStack {
            Text("Informacion")
                .font(.headline)
                .multilineTextAlignment(.leading)
                .padding(.vertical, 22.0)
           
            Text("Categoria: \(negocio.category.rawValue)")
                .font(.body)
                .multilineTextAlignment(.leading)
             
            Text("Tiempo promedio de espera: \((negocio.tiempo + 67)/60) minutos")
                .font(.body)
                .multilineTextAlignment(.leading)
        }
    }
}

struct InformacionView_Previews: PreviewProvider {
    static var previews: some View {
       Text("")
    }
}
