//
//  restaurantes.swift
//  cityUApp
//
//  Created by monica bayona on 27/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI

struct Restaurantes: View {

  @EnvironmentObject var session: FirebaseSession

 
 var categoria: String

   
 var body: some View {
     VStack(alignment: .leading) {
         HStack {
             Text("Restaurantes")
                 .font(.headline)
                 .padding(.leading, 15)
                 .padding(.top, 5)
             NavigationLink(
                 destination: categoryRestaurantView()
             ) {
                 Text("Ver todos")
             }
         }
         
        ScrollView(.vertical, showsIndicators: false) {
             HStack(alignment: .top, spacing: 0) {
                 ForEach(session.items.keys.sorted(), id: \.self) { index in
                    
                     HStack {
                        
                        if ( self.session.items[index]!.category.rawValue == self.categoria) {
                   NavigationLink(
                       destination: NegocioDetail(
                         negocio: self.session.items[index]!
                       )
                   ) {
                     CategoryItem(   negocio: self.session.items[index]!)
                   }
                         
                        }
                        
                    }
                 }
                     
                 }
                 
             
             }
         }
         .frame(height: 185)
     }
 }

struct restaurantes_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
