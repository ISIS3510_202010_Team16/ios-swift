//
//  NegocioDetail.swift
//  cityUApp
//
//  Created by monica bayona on 12/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI

struct NegocioDetail: View {
    var negocio: Negocio
    @State var stateVar: Int = 4
    @Environment(\.imageCache) var cache: ImageCache
      @EnvironmentObject var session: FirebaseSession
    var body: some View {
        
        VStack {
            
            Image("Grupo 8773 azul").resizable().padding(.bottom, 0.0).frame(height: 220.0).edgesIgnoringSafeArea(.all)
            
            VStack(alignment: .center, spacing: 0.0) {
                Text(negocio.name)
                    .font(.title)
                 
                Text("Tiempo de Espera: \(negocio.tiempo/60) minutos ")
                    .font(.headline)
                    .foregroundColor(/*@START_MENU_TOKEN@*/Color("naranjaDark")/*@END_MENU_TOKEN@*/)
                    
                    .padding(.vertical, 17.0)
                 
                
                HStack(alignment: .top) {
                    Button(action: { self.setState(state: 1)
                        
                    }) {
                        Text("Promociones ").font(.subheadline).foregroundColor(/*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                    }
                    
                    
                    Button(action: { self.setState(state: 3)
                        
                    }) {
                        Text("Calificaciones  ").font(.subheadline).foregroundColor(/*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                    }
                    
                    Button(action: { self.setState(state: 4)
                        
                    }) {
                        Text("Informacion  ").font(.subheadline).foregroundColor(/*@START_MENU_TOKEN@*/.gray/*@END_MENU_TOKEN@*/)
                    }

                }
                .padding(.bottom, 15.0)
                 
                Group {
                               if stateVar == 1 {
                                PromocionRows(negocio: negocio.id)
                       
                    }
                               else if stateVar == 2{
                                
                                
                                
                    }
                    else if stateVar == 3{
                                CalificacionesRows(negocio: negocio.id)
                                
                                
                                VStack {
                                               
                                        Button(action: {}) {
                                                                          NavigationLink(destination: CalificacionFormView()) {
                                                                                                       Text("         Añadir Calificacion         ").foregroundColor(Color(.white))
                                                                                                       .padding()
                                                                                                      
                                                                                                    }
                                                                             
                                                                      }
                                                                      .background(RoundedRectangle(cornerRadius: 20   , style: .continuous).foregroundColor(Color(red: 0.225, green: 0.718, blue: 0.766)))
                                           
                                           
                                              }
                                
                               
                                
                    }
                    else if stateVar == 4{
                                
                                InformacionView(negocio: negocio)
                                
                    }
                    
                    
                }
                .padding(.vertical, 10.0)
                
                
                Spacer()
            }.padding(.vertical, 0.0).frame(height: 600.0).background(Color(.white)).cornerRadius(15).edgesIgnoringSafeArea([.leading, .bottom, .trailing])
            
            Spacer()
             
        }.onAppear(perform: {self.session.restaurante = self.negocio.id})
       
    }
    
    func setState(state:    Int){
       stateVar = state
    }
}

struct NegocioDetail_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
