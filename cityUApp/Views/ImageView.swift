//
//  ImageView.swift
//  cityUApp
//
//  Created by Vadym Bulavin on 2/13/20.
//  Copyright © 2020 Vadym Bulavin. All rights reserved.
//
//

import Combine
import UIKit
import SwiftUI
import FirebaseStorage


struct AsyncImage<Placeholder: View>: View {
    @ObservedObject private var loader: ImageLoader
    private let placeholder: Placeholder?
    
    
    init(url: URL, cache: ImageCache? = nil, placeholder: Placeholder? = nil) {
        loader = ImageLoader(url: url, cache: cache)
        self.placeholder = placeholder
    }
    
    var body: some View {
        image
            .onAppear(perform: loader.load)
            
    }
    
    private var image: some View {
        Group {
            if loader.image != nil {
               Image(uiImage: loader.image!).resizable()
            } else {
                Image( placeholder as! String ).resizable()
            }
        }
    }
}

protocol ImageCache {
    subscript(_ url: URL) -> UIImage? { get set }
}

struct TemporaryImageCache: ImageCache {
    private let cache = NSCache<NSURL, UIImage>()
    
    subscript(_ key: URL) -> UIImage? {
        get { cache.object(forKey: key as NSURL) }
        set { newValue == nil ? cache.removeObject(forKey: key as NSURL) : cache.setObject(newValue!, forKey: key as NSURL) }
    }
}

class ImageLoader: ObservableObject {
    @Published var image: UIImage?
    
    private(set) var isLoading = false
    
    private let url: URL
    private var cache: ImageCache?
    private var cancellable: AnyCancellable?
    
     
    init(url: URL, cache: ImageCache? = nil) {
        self.url = url
        self.cache = cache
    }
    

    func load() {
        guard !isLoading else { return }

        if let image = cache?[url] {
            self.image = image
            return
        }
        
        let url2 = "images/\(url.absoluteString)"
              let storage = Storage.storage()
              let ref = storage.reference().child(url2)
              ref.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if data == nil {
                   return
                  }
                

                  DispatchQueue.main.async {
                    if data != nil {
                        let imageNew = UIImage(data: data!)
                      self.image = imageNew
                      self.cache(imageNew)
                      }
                    else{
                        
                    }
                  }
              }
        // Create local filesystem URL
      
        
        
        
       
        print(self.$image)
    }
    
  

    private func cache(_ image: UIImage?) {
        image.map { cache?[url] = $0 }
    }
}

struct ImageCacheKey: EnvironmentKey {
    static let defaultValue: ImageCache = TemporaryImageCache()
}

extension EnvironmentValues {
    var imageCache: ImageCache {
        get { self[ImageCacheKey.self] }
        set { self[ImageCacheKey.self] = newValue }
    }
}
