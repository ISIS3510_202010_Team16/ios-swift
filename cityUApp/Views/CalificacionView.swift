//
//  PedidoView.swift
//  cityUApp
//
//  Created by monica bayona on 25/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import UIKit
import Eureka
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SwiftUI

struct CalificacionFormView: View {
    
    @EnvironmentObject var session: FirebaseSession

    
    var body: some View {
     
        
        VStack {
      
        
             Image("Grupo 8773 azul").resizable().frame(height: 150.0).edgesIgnoringSafeArea(.all)
           
            
            
        CalificacionFormController().padding(.all, 13.0)
            
      Spacer()
        
    }
}

struct PedidoView_Previews: PreviewProvider {
    static var previews: some View {
            Text("")
    }}
}

struct CalificacionFormController: UIViewControllerRepresentable {
    
   

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: Context) -> calificacionFormViewController {
        let imagePickerController = calificacionFormViewController()
        
        return imagePickerController
    }
    
    func updateUIViewController(_ uiViewController: calificacionFormViewController, context: Context) {
        
    }
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var parent: CalificacionFormController
        
        init(_ imagePickerController: CalificacionFormController) {
            self.parent = imagePickerController
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            picker.dismiss(animated: true, completion: nil)
        }
    }
}






class calificacionFormViewController: FormViewController {
    
    struct FormItems {
        static let descripcion = "descripcion"
        static let titulo = "titulo"
        
        static let calificacion = "calificacion"
       
    }
    
 
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let required = RuleRequired<String>(msg: "Ingrese un valor")
        var min6 = RuleMinLength(minLength: 6);
                     min6.validationError = ValidationError(msg: "Debe contener al menos 6 caracteres.")
        var min2 = RuleMinLength(minLength: 2);
                            min2.validationError = ValidationError(msg: "Debe contener al menos 2 caracteres.")
               var max25 = RuleMaxLength(maxLength: 25);
               max25.validationError = ValidationError(msg: "Debe contener menos de 25 caracteres.")
        var max30 = RuleMaxLength(maxLength: 30);
        max30.validationError = ValidationError(msg: "Debe contener menos de 30 caracteres.")
        
        
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        form +++ Section("")
            
           
            <<< TextRow(FormItems.titulo) {
                $0.title = "Titulo"
                $0.add(rule: required)
                $0.add(rule: min2)
                $0.add(rule: max25)
                $0.validationOptions = .validatesOnChange
                }
                .cellUpdate { cell, row in
                    if !row.isValid {
                        cell.titleLabel?.textColor = .red
                    }
                }
                .onRowValidationChanged { cell, row in
                    let rowIndex = row.indexPath!.row
                    while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                        row.section?.remove(at: rowIndex + 1)
                    }
                    if !row.isValid {
                        for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                            let labelRow = LabelRow() {
                                $0.title = validationMsg
                                $0.cell.height = { 60 }
                            }.cellUpdate { cell, _ in
                                cell.textLabel?.numberOfLines = 2
                                cell.detailTextLabel?.numberOfLines = 2
                            }
                            let indexPath = row.indexPath!.row + index + 1
                            row.section?.insert(labelRow, at: indexPath)
                        }
                    }
            }
           
           
            
            <<< TextAreaRow(FormItems.descripcion) {
                    $0.placeholder = "Descripcion"
                    $0.textAreaHeight = .dynamic(initialTextViewHeight: 110)
            }
            
    
                   
            <<< PickerInputRow<Int>(FormItems.calificacion){
                        $0.title = "Calificacion"
                        $0.options = []
                        for i in 1...5{
                            $0.options.append(i)
                        }
                        $0.value = $0.options.first
                    }
    
            
        <<< ButtonRow { row in
            row.title = "Enviar Calificacion"
            }.cellSetup() {cell, row in
                
              cell.tintColor = UIColor.blue
            }.onCellSelection({ [unowned self] (cell, row) in
                row.section?.form?.validate();
                if let titulo1 = self.form.rowBy(tag: FormItems.titulo) as? RowOf<String>,
                    let titulo = titulo1.value,
                    let calificacion1 = self.form.rowBy(tag: FormItems.calificacion) as? RowOf<String>,
                    let calificacion = calificacion1.value,
                    let descripcion1 = self.form.rowBy(tag: FormItems.descripcion) as? RowOf<String>,
                    let descripcion = descripcion1.value
                  
            
                    
                    
                {
                    
                    if self.form.validate().isEmpty {
                        
                       
                        
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                            
                                                     let session = appDelegate.sessionManager
                           session?.registrarCalificacion(titulo: titulo, descripcion: descripcion, calificacion: calificacion)

                            
                    
                                                  }
                    
                        
                  }
                
                   
                    
                    
                    
                    
                    
                    
                  //  birthDateRow.value = Date(timeInterval: -900*365*86400, since: Date())
                 //   birthDateRow.updateCell()
                    
                  //  likeRow.value = true
                 //   likeRow.updateCell()
                    
                //    row.disabled = .function([FormItems.name]) { form in
               //         (form.rowBy(tag: FormItems.name) as? RowOf<String>)?.value == "Yoda"
              //      }
              //      row.evaluateDisabled()
                }
                    
            } )

                    
                    
                    
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



