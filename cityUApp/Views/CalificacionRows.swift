//
//  PromocionRows.swift
//  cityUApp
//
//  Created by monica bayona on 25/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI

struct CalificacionesRows: View {
   
     @EnvironmentObject var session: FirebaseSession
   var negocio: String
      
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Calificaciones")
                    .font(.headline)
                    .padding(.leading, 15)
                    .padding(.top, 5)
                
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 0) {
                    ForEach(session.calificaciones.keys.sorted(), id: \.self) { index in
                        HStack {
                            
                            if (self.session.calificaciones[index]!.restaurante == self.negocio) || self.negocio == "ALL" {
                           
                       CategoryCalificacionItem(   calificacion: self.session.calificaciones[index]!)
                            
                            }
                            
                            else{}
                            
                        }
                    }
                        
                    }
                    
                
                }
            }
            .frame(height: 185)
        }
}

struct CategoryCalificacionItem: View {
    var calificacion: Calificacion
    var body: some View {
        VStack(alignment: .leading) {
           
            Text(calificacion.titulo)
                .foregroundColor(.black)
                .font(.headline)
            Text(calificacion.descripcion)
                .foregroundColor(.gray)
                .font(.callout)
        }.frame(width: 200)
        .padding(.leading, 15)
    }
}

struct CalificacionRows_Previews: PreviewProvider {
    static var previews: some View {
        PromocionRows( negocio: "ALL")
    }
}
