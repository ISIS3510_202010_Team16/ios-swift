//
//  categoryRestaurantView.swift
//  cityUApp
//
//  Created by monica bayona on 12/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI


var lista = [CategoryItemObject(nombre: "Restaurantes", nombre2: "restaurantes", imageURL : "restaurantes" ),CategoryItemObject(nombre: "Bares", nombre2: "bares", imageURL : "bares" ),CategoryItemObject(nombre: "Supermercados", nombre2: "supermercados", imageURL : "supermercados" ),CategoryItemObject(nombre: "Postres", nombre2: "postres", imageURL : "postres" ),CategoryItemObject(nombre: "Cafes", nombre2: "cafes", imageURL : "cafes" )]


struct categoryRestaurantView: View {
    @EnvironmentObject var session: FirebaseSession
   
    var body: some View {
        
              
             
               VStack {  ZStack {
                           
                               Image("Grupo 8773").resizable().frame(height: 150.0).edgesIgnoringSafeArea(.all)
                               
                               Text("Categorias")
                                   .font(.title)
                                   .fontWeight(.semibold)
                                   .foregroundColor(Color.white).frame( height: 90 )
                               
                               
                               
                           
                               
               }.frame(height: 90.0)
               
                HStack(alignment: .top) {
                     ZStack {
                                  Image("restaurantes")
                                  .resizable()
                                                             .frame(width: 180.0, height: 110.0).background(Color(.white)).cornerRadius(15)
                        Text("Restaurantes")
                            .font(.subheadline)
                            .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                            .multilineTextAlignment(.leading)}
                    ZStack {
                      Image("bares")
                           .resizable()
                                                 .frame(width: 180.0, height: 110.0).background(Color(.white)).cornerRadius(15)
                        Text("Bares")
                            .font(.subheadline)
                            .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                            .multilineTextAlignment(.leading)}
                }
                HStack {
                    ZStack {
                                  Image("supermercados") .resizable()
                                                         .frame(width: 180.0, height: 110.0).background(Color(.white)).cornerRadius(15)
                        Text("Supermercados")
                            .font(.subheadline)
                            .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                            .multilineTextAlignment(.leading)}
                    ZStack {
                      Image("postres")
                          .resizable()
                        .frame(width: 180.0, height: 110.0).background(Color(.white)).cornerRadius(15)
                        Text("Postres")
                            .font(.subheadline)
                            .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                            .multilineTextAlignment(.leading)}
                }
                HStack {
                    ZStack {
                                  Image("cafes")
                                       .resizable()
                                                             .frame(width: 180.0, height: 110.0).background(Color(.white)).cornerRadius(15)
                        Text("Cafes")
                            .font(.subheadline)
                            .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                            .multilineTextAlignment(.leading)}
                    ZStack {
                      Image("otros")
                          .resizable()
                                                 .frame(width: 180.0, height: 110.0).background(Color(.white)).cornerRadius(15)
                        Text("Otros")
                            .font(.subheadline)
                            .foregroundColor(/*@START_MENU_TOKEN@*/.white/*@END_MENU_TOKEN@*/)
                            .multilineTextAlignment(.leading)}
                }
                 Spacer()
                 Spacer()
                 Spacer()
                 
              
                }
         
    }




}

struct CategoryItemObject: Identifiable {
    let id = UUID()
    let nombre :    String
    let nombre2 :    String
    let imageURL: String
}

struct categoryRestaurantView_Previews: PreviewProvider {
    static var previews: some View {
        categoryRestaurantView()
    }
}
