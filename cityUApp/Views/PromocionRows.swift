//
//  PromocionRows.swift
//  cityUApp
//
//  Created by monica bayona on 25/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI

struct PromocionRows: View {
   
     @EnvironmentObject var session: FirebaseSession
   var negocio: String
      
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Promociones")
                    .font(.headline)
                    .padding(.leading, 15)
                    .padding(.top, 5)
               
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 0) {
                    ForEach(session.promociones.keys.sorted(), id: \.self) { index in
                        HStack {
                            
                            if (self.session.promociones[index]!.restaurante == self.negocio) || self.negocio == "ALL" {
                           
                      NavigationLink(
                        destination: PedidoView( promocion: self.session.promociones[index]!)
                      ) {
                        CategoryPromocionItem(   promocion: self.session.promociones[index]!).padding(.all, 3.0)
                      }
                            
                            }
                            
                            else{}
                            
                        }
                    }
                        
                    }
                    
                
                }
        }
        .padding(.top, 2.0)
            
        }
}

struct CategoryPromocionItem: View {
    var promocion: Promocion
    @EnvironmentObject var session: FirebaseSession
    var body: some View {
        VStack(alignment: .leading) {
            
            Image(String(Int.random(in: 0..<5)))
            .resizable().renderingMode(.original)
                                  .cornerRadius(15)
                                  .frame(width: 140.0, height: 110.0).background(Color(.white)).cornerRadius(15)
           
            Text(promocion.producto)
                .font(.headline)
                .foregroundColor(Color.black)
               
            Text(promocion.descripcion)
            .foregroundColor(.primary)
                .font(.body)
            Text("Valor: \(promocion.costo)")
                .foregroundColor(.gray)
            .font(.caption)
            Text("Pidelo")
                .font(.caption)
                .foregroundColor(Color("azulDark"))
        }
        .onAppear(perform: {self.session.getData()}).frame( width: 140.0)
    }
}

struct PromocionRows_Previews: PreviewProvider {
    static var previews: some View {
        PromocionRows( negocio: "ALL")
    }
}
