//
//  LoginController.swift
//  cityUApp
//
//  Created by monica bayona on 3/2/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import UIKit
import Eureka
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SwiftUI

struct LoginView: View {
   
      @EnvironmentObject var session: FirebaseSession
    @State private var actionState: Int? = 0
    
    
    var body: some View {
        
       NavigationView {
                  
                   Group {
                    if session.isLoggedUsuario == false {
               
        
        ZStack {
        
            
                    
         Image("imageCity")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                 .edgesIgnoringSafeArea(.all)
         
            VStack() {
                
                Text("Iniciar Sesión").font(.title).fontWeight(.bold).foregroundColor(Color("azul")).multilineTextAlignment(.center).padding([.top, .leading, .trailing], 40.0)
            
                TestImagePickerController().padding([.leading, .bottom, .trailing], 13.0).frame(height: 170.0)
                
             //   NavigationLink(destination: HomeMenuUIView(),isActive: self.$session.isLoggedUsuario) {
            //      Text("")
           //     }.hidden()
         }
            .frame(width: 300.0).background(Color(.white)).cornerRadius(15).offset(y: -55)
         
            
                        }
                        
                    } else {
                        HomeMenuUIView().onAppear(perform: {self.session.getData()})
                                           .navigationBarItems(trailing: Button(action: {
                                                                     self.session.logOut()
                                                                 }) {
                                                                     Text("Cerrar Sesion")
                                                                 })
                                      }
                    
        }
        
        }
                        
    
    }
    
 
}



struct TestImagePickerController: UIViewControllerRepresentable {
    
   

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIViewController(context: Context) -> LoginViewController {
        let imagePickerController = LoginViewController()
        
        return imagePickerController
    }
    
    func updateUIViewController(_ uiViewController: LoginViewController, context: Context) {
        
    }
    
    class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        var parent: TestImagePickerController
        
        init(_ imagePickerController: TestImagePickerController) {
            self.parent = imagePickerController
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            picker.dismiss(animated: true, completion: nil)
        }
    }
}


class LoginViewController: FormViewController {
    
 
  
    struct FormItems {
           
              static let contraseña = "contraseña"
              static let email = "email"
          }
       
     
           
           
       
           
       override func viewDidLoad() {
           super.viewDidLoad()
           
           let required = RuleRequired<String>(msg: "Ingrese un valor")
                  var min6 = RuleMinLength(minLength: 6);
        
                               min6.validationError = ValidationError(msg: "Debe contener al menos 6 caracteres.")
                  var min2 = RuleMinLength(minLength: 2);
                                      min2.validationError = ValidationError(msg: "Debe contener al menos 2 caracteres.")
                         var max25 = RuleMaxLength(maxLength: 25);
                         max25.validationError = ValidationError(msg: "Debe contener menos de 25 caracteres.")
                  var max30 = RuleMaxLength(maxLength: 30);
                  max30.validationError = ValidationError(msg: "Debe contener menos de 30 caracteres.")
           
           
           tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
           
            form +++ Section("")
               
           <<< TextRow(FormItems.email) {
                           $0.title = "Email"
                           $0.add(rule: required)
                              $0.add(rule: min2)
                                 $0.add(rule: max30)
                           $0.validationOptions = .validatesOnChange
                           }
                           .cellUpdate { cell, row in
                               if !row.isValid {
                                   cell.titleLabel?.textColor = .red
                               }
                           }
                           .onRowValidationChanged { cell, row in
                               let rowIndex = row.indexPath!.row
                               while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                                   row.section?.remove(at: rowIndex + 1)
                               }
                               if !row.isValid {
                                   for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                                       let labelRow = LabelRow() {
                                           $0.title = validationMsg
                                           $0.cell.height = { 60 }
                                       }.cellUpdate { cell, _ in
                                           cell.textLabel?.numberOfLines = 2
                                           cell.detailTextLabel?.numberOfLines = 2
                                       }
                                       let indexPath = row.indexPath!.row + index + 1
                                       row.section?.insert(labelRow, at: indexPath)
                                   }
                               }
                       }
                      
                      
                    
                      
                  <<< PasswordRow(FormItems.contraseña) {
                      $0.title = "Contraseña"
                      $0.add(rule: min6)
                      $0.add(rule: max25)
                      $0.add(rule: required)
                      }
                      .cellUpdate { cell, row in
                          if !row.isValid {
                              cell.titleLabel?.textColor = .red
                          }
                      }
                      .onRowValidationChanged { cell, row in
                          let rowIndex = row.indexPath!.row
                          while row.section!.count > rowIndex + 1 && row.section?[rowIndex  + 1] is LabelRow {
                              row.section?.remove(at: rowIndex + 1)
                          }
                          if !row.isValid {
                              for (index, validationMsg) in row.validationErrors.map({ $0.msg }).enumerated() {
                                  let labelRow = LabelRow() {
                                      $0.title = validationMsg
                                      $0.cell.height = { 60 }
                                  }.cellUpdate { cell, _ in
                                      cell.textLabel?.numberOfLines = 2
                                      cell.detailTextLabel?.numberOfLines = 2
                                  }
                                  let indexPath = row.indexPath!.row + index + 1
                                  row.section?.insert(labelRow, at: indexPath)
                              }
                          }
                  }
                
           <<< ButtonRow { row in
        
                      row.title = "Iniciar sesion"
            
                      }.cellSetup() {cell, row in
                          
                        cell.tintColor = UIColor.blue
                      }.onCellSelection({ [unowned self] (cell, row) in
                          row.section?.form?.validate();
                          if let email2 = self.form.rowBy(tag: FormItems.email) as? RowOf<String>,
                              let email = email2.value,
                              let password2 = self.form.rowBy(tag: FormItems.contraseña) as? RowOf<String>,
                              let password = password2.value
                         
                   
                          {
                              
                              if self.form.validate().isEmpty {
                                
                                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                                    
                                                                 let session = appDelegate.sessionManager
                                   session?.logIn(email: email, password: password){ (result, error) in
                                       if result == nil {
                                        print("paso1")
                                        let alertController = UIAlertController(title: "No se pudo Iniciar Sesión", message: error?.localizedDescription, preferredStyle: .alert)
                                                                                                                    
                                                                                                                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                                                                                                    alertController.addAction(defaultAction)
                                                                                                                    
                                                                                                                  
                                                                                                                        if self.presentedViewController == nil {
                                                                                                                            self.present(alertController, animated: true, completion: nil)
                                                                                                                        }
                                                                                                                        else {
                                                                                                                            self.dismiss(animated: false, completion: nil)
                                                                                                                            self.present(alertController, animated: true, completion: nil)
                                                                                                                        }
                                       } else {
                                        print("paso2")
                                           session?.isLoggedUsuario = true
                                       }
                                   }
                                   
                                    
                                
                                    
                                   
                                        
                             
                                    
                                                              }
                              
                             
                                
                                
                            }
           
                            }
                                               
                                       } ) /*.cellSetup { cell, row in
                                        cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                                        cell.textLabel?.textAlignment = .center
                                            cell.imageView?.image = UIImage(named: "siguiente azul")
                                       
                                       
                                        
                                       }*/

                                               
                                               
                                               
                                   // Do any additional setup after loading the view.
                               }
                               

                               /*
                               // MARK: - Navigation

                               // In a storyboard-based application, you will often want to do a little preparation before navigation
                               override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                                   // Get the new view controller using segue.destination.
                                   // Pass the selected object to the new view controller.
                               }
                               */
    
    
}

struct LoginController_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
