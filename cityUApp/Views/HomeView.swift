//
//  HomeView.swift
//  cityUApp
//
//  Created by monica bayona on 4/04/20.
//  Copyright © 2020 monica bayona. All rights reserved.
//

import SwiftUI

struct HomeView: View {
    
      @EnvironmentObject var session: FirebaseSession
    var body: some View {
        
        NavigationView {
            
             Group {
                if session.isLoggedUsuario == false {
        

        ZStack {
          
           
                       
            Image("imageCity")
                       .resizable()
                       .aspectRatio(contentMode: .fill)
                    .edgesIgnoringSafeArea(.all)
            
            VStack {
                Image("logo").resizable()
                    .padding(.bottom, 100.0)
                    .scaledToFit()
                .frame(width: 250.0, height: 340.0)
                   
      VStack {
                
        Button(action: {}) {
            NavigationLink(destination: LoginView()) {
                                         Text("         Iniciar Sesión         ").foregroundColor(Color(.white))
                                         .padding()
                                        
                                      }
               
        }
        .background(RoundedRectangle(cornerRadius: 20   , style: .continuous).foregroundColor(Color(red: 0.225, green: 0.718, blue: 0.766)))
            
            
               }
            HStack {
                Text("No tienes usuario").foregroundColor(Color(.white))
                Button(action: {}) {
                    
                   
                            NavigationLink(destination: SignUpView()) {
                               Text("Registrate").foregroundColor(Color(red: 0.225, green: 0.718, blue: 0.766))
                              
                            }
                    
                    
                         
                    
                         
                }
            }
            }
    }.navigationBarHidden(true)
        }
                
                
                else {
                    HomeMenuUIView().onAppear(perform: {self.session.getData()})
                        .navigationBarItems(trailing: Button(action: {
                                                                     self.session.logOut()
                                                                 }) {
                                                                     Text("Cerrar Sesion")
                                                                 })
                   }
                
                
            }
        }.accentColor( .white)
        
        
    }
    
    
   
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
