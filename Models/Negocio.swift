/*
See LICENSE folder for this sample’s licensing information.

Abstract:
The model for an individual landmark.
*/

import SwiftUI
import CoreLocation

struct Negocio: Hashable, Codable {
    var id: Int
    var name: String
    var imageName: String
    var coordinates: Coordinates
    var tiempo: Int
    var calificacion: Int
    var category: Category
   
   
  var featureImage: Image? {
        return Image(
            ImageStore.loadImage(name: "\(imageName)_feature"),
            scale: 2,
            label: Text(name))
    }

    enum Category: String, CaseIterable, Codable, Hashable {
        case restaurante = "Restaurantes"
        case bar = "Bares"
        case postres = "Postres"
        case supermercado = "Supermercados"
    }
}



struct Coordinates: Hashable, Codable {
    var latitude: Double
    var longitude: Double
}

struct Negocio_Previews: PreviewProvider {
    static var previews: some View {
        /*@START_MENU_TOKEN@*/Text("Hello, World!")/*@END_MENU_TOKEN@*/
    }
}
